package integration;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ChargeBackMapperBoltTest {

    private OutputCollector output;
    private ArgumentCaptor<Values> argument;
    private Tuple input;
    private ChargeBackMapperBolt bolt;

    @BeforeEach
    void setup() {
        output = mock(OutputCollector.class);
        argument = ArgumentCaptor.forClass(Values.class);
        input = mock(Tuple.class);
        bolt = new ChargeBackMapperBolt();
        bolt.prepare(mock(HashMap.class), mock(TopologyContext.class), output);
    }

    @Test
    void execute() {

        // given
        String json = "{\"paymentId\":1,\"customerIdReference\":null,\"customerReference\":\"8961ec85-1ec4-4773-baf1-e35cf18f6e5a\",\"customerOrderIdReference\":null,\"customerOrderReference\":\"4e7d3768-27ec-43e0-a491-a87f3153fef8\",\"merchantIdReference\":null,\"namespace\":\"IFOODPAY_TIP_BR\",\"digitalWallet\":null,\"acquirerCode\":null,\"bin\":\"123456\",\"brand\":\"CARTAOCREDITO\",\"event\":\"CHARGEBACK\",\"notificationDate\":1569063957640,\"paymentDate\":1566089880612,\"value\":5.00,\"currencyCode\":\"BRL\",\"externalReferences\":{\"ZOP\":\"987654321\"},\"test\":false,\"eventType\":\"CHARGEBACK\"}";
        when(input.getStringByField("value")).thenReturn(json);

        // when
        bolt.execute(input);

        // then
        verify(output).emit(any(Tuple.class), argument.capture());
        assertThat(argument.getValue().get(0)).isEqualTo(1);
    }
}
