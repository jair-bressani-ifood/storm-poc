package hello;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;

public class HelloTopology {

    public static void main(String[] args) throws Exception {

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("country", new CountrySpout());

        builder.setBolt("hello", new HelloBolt()).shuffleGrouping("country");

        builder.setBolt("bye", new ByeBolt())
            .shuffleGrouping("country")
            .shuffleGrouping("hello");

        Config conf = new Config();
        conf.setDebug(true);

        String topologyName = "hello-country";

        if (args != null && args.length > 0) {
            topologyName = args[0];
        }

        StormSubmitter.submitTopology(topologyName, conf, builder.createTopology());
    }
}
