package basic;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class WordCountTopology {

    public static void main(String[] args) throws Exception {

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("spout", new RandomSentenceSpout(), 3);
        builder.setBolt("split", new SplitSentenceBolt(), 6).shuffleGrouping("spout");
        builder.setBolt("count", new WordCountBolt(), 9).fieldsGrouping("split", new Fields("word"));

        Config conf = new Config();
        conf.setDebug(true);
        conf.setNumWorkers(3);

        String topologyName = "word-count";

        if (args != null && args.length > 0) {
            topologyName = args[0];
        }

        StormSubmitter.submitTopology(topologyName, conf, builder.createTopology());
    }
}
