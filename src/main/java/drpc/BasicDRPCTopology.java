package drpc;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.drpc.LinearDRPCTopologyBuilder;

public class BasicDRPCTopology {

    public static void main(String[] args) throws Exception {

        LinearDRPCTopologyBuilder builder = new LinearDRPCTopologyBuilder("exclamation");
        builder.addBolt(new ExclaimBolt());

        Config conf = new Config();
        conf.setDebug(true);

        String topologyName = "basic-drpc";

        if (args != null && args.length > 0) {
            topologyName = args[0];
        }

        StormSubmitter.submitTopology(topologyName, conf, builder.createRemoteTopology());
    }
}
