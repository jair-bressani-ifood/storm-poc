# POC APACHE STORM

## Executando o Storm com instalação local 

### Local Mode
```
mvn clean package

/install-path-storm/bin/storm local target/storm-1.0-SNAPSHOT.jar integration.AggregateTopology --local-ttl 60
```

### Remote Mode
Depende do Zookeeper 
```
/install-path-zookeeper/bin/zkServer.sh start
/install-path-storm/bin/storm nimbus
/install-path-storm/bin/storm supervisor
/install-path-storm/bin/storm drpc
/install-path-storm/bin/storm ui
/install-path-storm/bin/storm logviewer
mvn clean package
/install-path-storm/bin/storm jar target/storm-1.0-SNAPSHOT.jar integration.AggregateTopology
```
- URL Storm UI: localhost:8080
- URL Storm Log viewer: localhost:8000

## Executando com Docker

**1) Levantando o cluster Storm**:
```
docker-compose up -d
```

**2) Levantando o Storm UI**:
- URL UI: localhost:8080
```
docker-compose exec nimbus storm ui
```

**3) Enviando topologia para o cluster**:
```
docker-compose exec nimbus bash
cd /workdir
storm jar target/storm-1.0-SNAPSHOT.jar basic.WordCountTopology
```

## Criando tabelas no PostgreSQL

Para funcionamento da POC de agregação, é necessario preencher as colunas da tabela `order_placed`, onde o campo `transaction_id` deve corresponder com o campo `paymentId` do JSON a ser publicado no Kafka (ver exemplo abaixo).  

```
create table order_placed (transaction_id integer, customer_id integer, restaurant_id integer, address_id integer);

create table chargeback_customer (customer_id integer unique, total integer);

create table chargeback_restaurant (restaurant_id integer unique, total integer);

create table chargeback_address (address_id integer unique, total integer);
```

## JSON utilizado na POC
```
{"paymentId":1,"customerIdReference":null,"customerReference":"8961ec85-1ec4-4773-baf1-e35cf18f6e5a","customerOrderIdReference":null,"customerOrderReference":"4e7d3768-27ec-43e0-a491-a87f3153fef8","merchantIdReference":null,"namespace":"IFOODPAY_TIP_BR","digitalWallet":null,"acquirerCode":null,"bin":"123456","brand":"CARTAOCREDITO","event":"CHARGEBACK","notificationDate":1569063957640,"paymentDate":1566089880612,"value":5.00,"currencyCode":"BRL","externalReferences":{"ZOP":"987654321"},"test":false,"eventType":"CHARGEBACK"}
```