package integration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import config.StormConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.jdbc.bolt.JdbcInsertBolt;
import org.apache.storm.jdbc.bolt.JdbcLookupBolt;
import org.apache.storm.jdbc.common.Column;
import org.apache.storm.jdbc.common.ConnectionProvider;
import org.apache.storm.jdbc.common.HikariCPConnectionProvider;
import org.apache.storm.jdbc.mapper.JdbcMapper;
import org.apache.storm.jdbc.mapper.SimpleJdbcLookupMapper;
import org.apache.storm.jdbc.mapper.SimpleJdbcMapper;
import org.apache.storm.kafka.spout.ByTopicRecordTranslator;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;
import org.apache.storm.kafka.spout.KafkaSpoutRetryExponentialBackoff;
import org.apache.storm.kafka.spout.KafkaSpoutRetryService;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import static org.apache.storm.kafka.spout.FirstPollOffsetStrategy.EARLIEST;

public class AggregateTopology {

    private static final String AGGREGATE_STREAM = "aggregate_stream";
    private static final String KAFKA_LOCAL_BROKER = "localhost:9092";
    private static final String KAFKA_TOPIC_0 = "kafka-spout-test";
    private static final String KAFKA_TOPIC_1 = "kafka-spout-test-1";
    private static final String KAFKA_TOPIC_2 = "kafka-spout-test-2";

    public static void main(String[] args) throws Exception {

        ApplicationContext context = new AnnotationConfigApplicationContext(StormConfig.class);
        Config config = context.getBean(Config.class);

        TopologyBuilder builder = new TopologyBuilder();

        // KAFKA: read charge-back topic
        builder.setSpout("charge_back_spout", new KafkaSpout<>(getKafkaSpoutConfig(KAFKA_LOCAL_BROKER)));

        // Extract transaction_id from JSON
        builder.setBolt("charge_back_mapper", new ChargeBackMapperBolt())
            .shuffleGrouping("charge_back_spout", AGGREGATE_STREAM);

        // DATABASE: find transaction data
        builder.setBolt("order_placed_data", getLookupBolt(getDataSource())).shuffleGrouping("charge_back_mapper");

        // DATABASE: update aggregated tables
        JdbcInsertBolt aggregateCustomerBolt =
            getInsertBolt(getConnection(getDataSource()), "chargeback_customer", "customer_id");
        JdbcInsertBolt aggregateRestaurantBolt =
            getInsertBolt(getConnection(getDataSource()), "chargeback_restaurant", "restaurant_id");
        JdbcInsertBolt aggregateAddressBolt =
            getInsertBolt(getConnection(getDataSource()), "chargeback_address", "address_id");

        builder.setBolt("aggregate_customer", aggregateCustomerBolt).shuffleGrouping("order_placed_data");
        builder.setBolt("aggregate_restaurant", aggregateRestaurantBolt).shuffleGrouping("order_placed_data");
        builder.setBolt("aggregate_address", aggregateAddressBolt).shuffleGrouping("order_placed_data");

        String topologyName = "charge-back-aggregation";

        if (args != null && args.length > 0) {
            topologyName = args[0];
        }

        StormSubmitter.submitTopology(topologyName, config, builder.createTopology());
    }

    private static Map<String, Object> getDataSource() {
        Map<String, Object> dataSource = Maps.newHashMap();
        dataSource.put("dataSourceClassName", "org.postgresql.ds.PGSimpleDataSource");
        dataSource.put("dataSource.url", "jdbc:postgresql://localhost:5432/postgres");
        dataSource.put("dataSource.user", "postgres");
        dataSource.put("dataSource.password", "postgres");
        return dataSource;
    }

    private static JdbcInsertBolt getInsertBolt(ConnectionProvider connectionProvider, String tableName,
                                                String columnName) {

        List<Column> schemaColumns = Lists.newArrayList(new Column(columnName, Types.INTEGER));
        JdbcMapper mapper = new SimpleJdbcMapper(schemaColumns);

        String query = "INSERT INTO " + tableName +
            " (" + columnName + ", total) " +
            " VALUES (?, 1) " +
            " ON CONFLICT (" + columnName + ") DO UPDATE SET total = " + tableName + ".total + 1";

        JdbcInsertBolt insertBolt = new JdbcInsertBolt(connectionProvider, mapper).withInsertQuery(query);

        return insertBolt;
    }

    private static JdbcLookupBolt getLookupBolt(Map<String, Object> dataSource) {

        String selectQuery = "select * from order_placed where transaction_id = ?";
        Fields outputFields = new Fields("transaction_id", "customer_id", "restaurant_id", "address_id");
        List<Column> queryParamColumns = Lists.newArrayList(new Column("transaction_id", Types.INTEGER));

        return new JdbcLookupBolt(
            getConnection(dataSource),
            selectQuery,
            new SimpleJdbcLookupMapper(outputFields, queryParamColumns));
    }

    private static ConnectionProvider getConnection(Map<String, Object> dataSource) {

        ConnectionProvider connectionProvider = new HikariCPConnectionProvider(dataSource);
        connectionProvider.prepare();

        return connectionProvider;
    }

    private static KafkaSpoutConfig<String, String> getKafkaSpoutConfig(String bootstrapServers) {

        ByTopicRecordTranslator<String, String> trans = new ByTopicRecordTranslator<>(
            (r) -> new Values(r.topic(), r.partition(), r.offset(), r.key(), r.value()),
            new Fields("topic", "partition", "offset", "key", "value"), AGGREGATE_STREAM);

        return KafkaSpoutConfig.builder(bootstrapServers, new String[]{KAFKA_TOPIC_0, KAFKA_TOPIC_1, KAFKA_TOPIC_2})
            .setProp(ConsumerConfig.GROUP_ID_CONFIG, "kafkaSpoutTestGroup")
            .setRetry(getRetryService())
            .setRecordTranslator(trans)
            .setOffsetCommitPeriodMs(10_000)
            .setFirstPollOffsetStrategy(EARLIEST)
            .setMaxUncommittedOffsets(250)
            .build();
    }

    private static KafkaSpoutRetryService getRetryService() {
        return new KafkaSpoutRetryExponentialBackoff(
            KafkaSpoutRetryExponentialBackoff.TimeInterval.microSeconds(500),
            KafkaSpoutRetryExponentialBackoff.TimeInterval.milliSeconds(2),
            Integer.MAX_VALUE,
            KafkaSpoutRetryExponentialBackoff.TimeInterval.seconds(10)
        );
    }
}
