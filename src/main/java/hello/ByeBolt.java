package hello;

import basic.WordCountBolt;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ByeBolt extends BaseRichBolt {

    private static final Logger logger = LoggerFactory.getLogger(WordCountBolt.class);
    private OutputCollector output;

    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        output = outputCollector;
    }

    public void execute(Tuple input) {
        String country = input.getString(0);
        String helloMessage = "Bye, bye " + country;
        output.emit(input, new Values(helloMessage));
        output.ack(input);
        logger.info("ByeBolt: {}", helloMessage);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("bye"));
    }
}
