package hello;

import basic.WordCountBolt;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Random;

public class CountrySpout extends BaseRichSpout {

    private static final Logger logger = LoggerFactory.getLogger(CountrySpout.class);
    private SpoutOutputCollector output;

    public void open(Map<String, Object> map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
        output = spoutOutputCollector;
    }

    public void nextTuple() {
        Utils.sleep(100);
        String[] countries = new String[]{"Brasil", "Bolivia", "USA", "Mexico"};
        Random rand = new Random();
        String country = countries[rand.nextInt(countries.length)];
        output.emit(new Values(country));
        logger.info("CountrySpout: {}", country);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("country"));
    }
}
