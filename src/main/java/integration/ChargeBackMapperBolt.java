package integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class ChargeBackMapperBolt extends BaseRichBolt {

    private static final Logger logger = LoggerFactory.getLogger(ChargeBackMapperBolt.class);
    private OutputCollector output;
    private ObjectMapper mapper = new ObjectMapper();

    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        output = outputCollector;
    }

    public void execute(Tuple input) {
        try {
            String jsonString = input.getStringByField("value");
            Map<String, Object> chargeBackData = mapper.readValue(jsonString, new TypeReference<Map<String, Object>>(){});

            Integer paymentId = (Integer) chargeBackData.getOrDefault("paymentId", 0);

            logger.info("ChargeBackMapperBolt: {}", paymentId);
            output.emit(input, new Values(paymentId));

        } catch (IOException e) {
            // show up the error in storm UI
            output.reportError(e);
            logger.error(e.getMessage(), e);
        } finally {
            output.ack(input);
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("transaction_id"));
    }
}
