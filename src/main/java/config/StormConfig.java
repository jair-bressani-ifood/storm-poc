package config;

import com.google.common.collect.Maps;
import org.apache.storm.Config;
import org.apache.storm.jdbc.common.ConnectionProvider;
import org.apache.storm.jdbc.common.HikariCPConnectionProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;

@Configuration
@PropertySource("classpath:config.properties")
public class StormConfig {

    @Value("${datasource.classname}")
    private String dsClassName;
    @Value("${datasource.url}")
    private String dsUrl;
    @Value("${datasource.user}")
    private String dsUser;
    @Value("${datasource.password}")
    private String dsPassword;

    private Map<String, Object> getDataSource() {
        Map<String, Object> dataSource = Maps.newHashMap();
        dataSource.put("dataSourceClassName", dsClassName);
        dataSource.put("dataSource.url", dsUrl);
        dataSource.put("dataSource.user", dsUser);
        dataSource.put("dataSource.password", dsPassword);
        return dataSource;
    }

    @Bean
    public ConnectionProvider getConnection() {
        ConnectionProvider connectionProvider = new HikariCPConnectionProvider(getDataSource());
        connectionProvider.prepare();
        return connectionProvider;
    }

    @Bean
    public Config getConfig() {
        Config conf = new Config();
        conf.setDebug(true);
        conf.put("jdbc.conf", getDataSource());
        return conf;
    }
}
