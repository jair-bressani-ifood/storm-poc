package basic;

import integration.ChargeBackMapperBolt;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class WordCountBolt extends BaseRichBolt {

    private static final Logger logger = LoggerFactory.getLogger(WordCountBolt.class);
    private OutputCollector output;
    private Map<String, Integer> counts = new HashMap<>();

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        output = collector;
    }

    @Override
    public void execute(Tuple input) {

        String word = input.getStringByField("word");

        Integer count = counts.getOrDefault(word, 0);
        counts.put(word, ++count);

        output.emit(new Values(word, count));
        output.ack(input);
        logger.info("WordCountBolt: {} = {}", word, count);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("word", "count"));
    }
}
