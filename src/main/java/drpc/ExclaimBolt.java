package drpc;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ExclaimBolt extends BaseBasicBolt {

    private static final Logger logger = LoggerFactory.getLogger(ExclaimBolt.class);

    public void execute(Tuple input, BasicOutputCollector output) {
        Object key = input.getValue(0);
        String value = "WHAT A F... " + input.getValue(1) + " !!!!";
        output.emit(new Values(key, value + "!"));
        logger.info("ExclaimBolt: id = {} / result = {}", key, value);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("id", "result"));
    }
}