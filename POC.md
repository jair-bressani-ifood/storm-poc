Soma histórica, a partir do momento que estiver agregando:
- chargeback por customer
- chargeback por address
- chargeback por device
- chargeback por restaurant

Topico do Kafka: ANTIFRAUD_PAYMENT_CHARGEBACK

Payload:
```{
   "paymentId":123456789,
   "customerIdReference":null,
   "customerReference":"8961ec85-1ec4-4773-baf1-e35cf18f6e5a",
   "customerOrderIdReference":null,
   "customerOrderReference":"4e7d3768-27ec-43e0-a491-a87f3153fef8",
   "merchantIdReference":null,
   "namespace":"IFOODPAY_TIP_BR",
   "digitalWallet":null,
   "acquirerCode":null,
   "bin":"123456",
   "brand":"CARTAOCREDITO",
   "event":"CHARGEBACK",
   "notificationDate":1569063957640,
   "paymentDate":1566089880612,
   "value":5.00,
   "currencyCode":"BRL",
   "externalReferences":{
      "ZOP":"987654321"
   },
   "test":false,
   "eventType":"CHARGEBACK"
}```